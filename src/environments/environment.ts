// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  // baseUrl: 'https://dev-api.dfstars.com',
  // baseUrl: 'https://test-api.dfstars.com',
  baseUrl: 'https://api.dfstars.com',
  housePlayers: ["s7test01", "s7test02", "s7test03", "s7test04", "s7test05"],
  housePlayersPassword: "aa123456"

};
