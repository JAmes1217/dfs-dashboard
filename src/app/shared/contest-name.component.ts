import { Component, OnInit, Input } from '@angular/core';
import { Contest } from 'app/model/contest.model';

@Component({
  selector: 'app-contest-name',
  template: `
     <img  *ngIf="contest?.leagueId == 1"  src="https://cdn1.iconfinder.com/data/icons/international-circular-flags/512/united_states_usa_america_flag_circle-128.png" height="16" alt="">
      <img  *ngIf="contest?.leagueId == 99999"  src="https://cdn1.iconfinder.com/data/icons/international-circular-flags/512/united_states_usa_america_flag_circle-128.png" height="16" alt="">
      <img  *ngIf="contest?.leagueId == 12"  src="https://cdn0.iconfinder.com/data/icons/world-flags-6-2/100/England-128.png" height="16" alt="">
      <img  *ngIf="contest?.leagueId == 3"  src="https://www.shareicon.net/data/128x128/2016/08/18/815520_world_512x512.png" height="16" alt="">
      <img  *ngIf="contest?.leagueId == 21"  src="img/south-korea.png" height="16" alt="">
      {{ getLeagueName()   }} {{ contest.prizeStructure  }} {{ tournamentName }} 

  
      <i class="fa fa-lock" *ngIf="contest?.privateContest"></i>
  `
})
export class ContestNameComponent implements OnInit {
  @Input() contest: Contest;
  contestName: string;
  tournamentName: string;
  constructor() { }

  ngOnInit() {
    this.tournamentName = (this.contest.tournament) ? "TOURNAMENT" : "NON_TOURNAMENT";
  }

  getLeagueName() {
    if (this.contest.leagueId == 1) {
      return "NBA";
    } else if (this.contest.leagueId == 12) {
      return "EPL";
    } else if (this.contest.leagueId == 3) {
      return "CSL"
    } else if (this.contest.leagueId == 21) {
      return "LCK"
    } else if (this.contest.leagueId == 11) {
      return "UEFA"
    }
     else {
      return "TEST";
    }
  }
}