import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContestListHousePlayersComponent } from 'app/house-players/contest-list/contest-list.house-players.component';
import { ContestResolve } from 'app/contest/contest.resolver';
import { CreateLineupHousePlayersComponent } from 'app/house-players/create-lineup/create-lineup.house-players.component';



const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ContestListHousePlayersComponent,
        data: {
          title: 'Contest List'
        }
        
      },
      {
        path: 'enter/:id',
        component: CreateLineupHousePlayersComponent,
        data: {
          title: 'Create Lineup'
        },
        resolve: {
          contest: ContestResolve
        }
      }
      
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HousePlayersRoutingModule { }