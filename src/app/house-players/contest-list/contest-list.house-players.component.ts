import { Component, OnInit } from '@angular/core';
import { ContestService } from '../../services/contest.service';
import { Observable } from 'rxjs/Observable';
import { Contest } from '../../model/contest.model';
import { LeagueService } from 'app/services/league.service';
import { League } from 'app/model/league.model';

@Component({
  selector: 'app-contest-list-hp',
  templateUrl: './contest-list.house-players.component.html',
  styleUrls: ['./contest-list.house-players.component.scss']
})
export class ContestListHousePlayersComponent implements OnInit {
  public contests: Observable<Contest []>;
  public leagues: Observable<League []>;
  public selectedLeague: League;
  public selectedPeriod: number;
  public currentPage: number;
  constructor(
    private contestService: ContestService,
    private leagueService: LeagueService
  ) {
    this.selectedLeague = null;
    
    this.contests = this.contestService.subscribeContestList();
    this.leagues = this.leagueService.leagueList();
    this.leagueService.loadAllLeagues();
  }

  ngOnInit() {
    this.currentPage = 1;
   }

   filterList() {
     this.currentPage = 1; 
     this.loadContest();
   }

   nextPage() {
     const page = this.currentPage + 1;
     this.goToPage(page);
   }
   
   private goToPage(page: number) {
    this.currentPage = page;
    this.loadContest();
   }

   private loadContest() {
     console.log(this.selectedLeague.id);
     console.log(this.selectedPeriod);
     this.contestService.fetchContests(this.selectedLeague.id, this.selectedPeriod);

   }
}