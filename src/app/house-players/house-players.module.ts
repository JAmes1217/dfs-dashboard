import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContestListHousePlayersComponent } from 'app/house-players/contest-list/contest-list.house-players.component';
import { HousePlayersRoutingModule } from 'app/house-players/house-players-routing.module';
import { ContestService } from 'app/services/contest.service';
import { LeagueService } from 'app/services/league.service';
import { FormsModule } from '@angular/forms';
import { ContestNameComponent } from 'app/shared/contest-name.component';
import { ContestResolve } from 'app/contest/contest.resolver';
import { CreateLineupHousePlayersComponent } from 'app/house-players/create-lineup/create-lineup.house-players.component';
import { PlayerService } from 'app/services/player.service';
import { HousePlayersService } from 'app/services/house-players.service';
import { ModalModule } from 'ngx-bootstrap';
import { OrderModule } from 'ngx-order-pipe';

@NgModule({
  declarations: [ContestListHousePlayersComponent, CreateLineupHousePlayersComponent, ContestNameComponent],
  imports: [ CommonModule, FormsModule, HousePlayersRoutingModule, ModalModule.forRoot(), OrderModule],
  exports: [],
  providers: [ContestService, LeagueService, ContestResolve, PlayerService, HousePlayersService],
})
export class HousePlayersModule {}