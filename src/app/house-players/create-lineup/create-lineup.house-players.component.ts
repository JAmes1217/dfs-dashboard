import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Contest } from 'app/model/contest.model';
import { PlayerService } from 'app/services/player.service';
import { Player } from 'app/model/player.model';
import { LeagueService } from 'app/services/league.service';
import { HousePlayersService } from 'app/services/house-players.service';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-create-lineup-house-players',
  templateUrl: './create-lineup.house-players.component.html',
  styleUrls: ['./create-lineup.house-players.component.scss']
})
export class CreateLineupHousePlayersComponent implements OnInit {
  public contest: Contest;
  public availablePlayerList: Player [];
  public draftList: Player [];
  public salaryCap: number;
  public slots: any;
  public positions: any;
  public playerList: any;
  public lineupComplete:boolean;
  public housePlayers: string;
  public totalHousePlayers: number;
  public finishedHousePlayers: number;
  @ViewChild("modalLoading") modalLoading;
  public modalRef: BsModalRef;
  public sortAsc: boolean;
  public selectedSortField: string;
  public selectedPosition;

  constructor(
    private route: ActivatedRoute,
    private playerService: PlayerService,
    private leagueService: LeagueService,
    private housePlayerService: HousePlayersService,
    private modalService: BsModalService
  ) {
    this.contest = this.route.snapshot.data['contest'];
    this.playerList = [];
    this.totalHousePlayers = 0;
    this.housePlayerService
        .subscribeFinishedSubmission()
        .subscribe(finished => {
          this.finishedHousePlayers = finished;

          // if(this.finishedHousePlayers == this.totalHousePlayers) {
            
          // }
        });
    }


  ngOnInit() { 
    this.sortAsc = true;
    this.selectedSortField = "salary";
    this.lineupComplete = false;
    this.salaryCap = 50000;
    this.playerList = [];
  
    this.leagueService
      .getLeagueByLeagueId(this.contest.leagueId)
      .subscribe(league => {
        this.slots = league.settings.lineupSlots;
        this.positions = league.settings.positions;
        // this.selectedPosition = this.positions[0];
        this.loadPlayers();
      })
  }


  addPlayer(player: Player, slotObject: any = false) {
   
    if (player) {
      let slot = (slotObject) ? slotObject : this.getAvailableSlot(player);

      if (slot) {
        this.playerList[slot.id] = { player: player, slot: slot }

        for (var x in this.availablePlayerList) {
          if (player.id == this.availablePlayerList[x].id) {
            this.availablePlayerList.splice(parseFloat(x), 1);
          }
        }

        this.salaryCap -= player.salary;
        this.refreshDraftBox();


      } else {
        
      }

    }

  }

  removePlayer(player, slotId) {
    delete this.playerList[slotId];
    player.positionCode = (player.positionCode) ? player.positionCode : player.position.code;
    this.availablePlayerList.push(player);
    this.salaryCap += player.salary;
    
    this.refreshDraftBox();
  }

  sort(fieldName: string, triggerArrangementOrder: boolean = true) {
    this.sortAsc = (triggerArrangementOrder) ? !this.sortAsc : this.sortAsc;
    this.selectedSortField = fieldName;
  
  }

  selectPosition(position) {
    this.selectedPosition = position;
    this.filterDraftList();
  }

  filterDraftList() {
    console.log(this.selectedPosition);
    let actualPosition = this.selectedPosition.actualPositions.map(position => { return position.id });
    this.draftList = this.availablePlayerList.filter(player => { return actualPosition.includes(player.positionId) });
   

    this.sort(this.selectedSortField, false);

  } 

  submit (modalLoading) {
    // console.log(this.availablePlayerList);
    let players = this.playerList.filter(player => { return player != null });
    let housePlayers = this.buildHousePlayersArray(this.housePlayers);
    this.totalHousePlayers = housePlayers.length;
    this.modalRef = this.modalService.show(modalLoading);
    this.housePlayerService.join(this.contest.id, housePlayers, players, this.availablePlayerList);

  }

  cancel() {
    this.modalRef.hide();
  }

  private loadPlayers() {
    this.playerService
      .getPlayersByLeagueId(this.contest.leagueId)
      .subscribe(players => {
        this.availablePlayerList = players;
        this.draftList = this.availablePlayerList;
      })

  }

  private buildHousePlayersArray(housePlayers) {
    let housePlayersArray = [];
    let segmentedHp = housePlayers.split(",");
    segmentedHp.forEach(element => {
      element = element.trim();
      if (element.indexOf('-') > -1) {
        let rangeArray = element.split('-');
        for (let index = rangeArray[0]; index <= Number(rangeArray[1]); index++) {
          housePlayersArray.push(index);
        }
      } else {
        housePlayersArray.push(Number(element));
      }
    });

    return housePlayersArray;
  }

  private getAvailableSlot(player) {
    for (let i = 0; i < this.slots.length; i++) {
      let slot = this.slots[i];
      if (this.playerList[slot.id] != null) { continue; }
      for (let ii = 0; ii < slot.actualPositions.length; ii++) {
        let actualPosition = slot.actualPositions[ii];

        if (actualPosition.id == player.positionId) { return slot; }
      }
    }
  }

  private refreshDraftBox() {

    let playerCount = 0;
    for (var x in this.playerList) {
      if (this.playerList[x]) { ++playerCount; }
    }
    this.lineupComplete = (playerCount == this.slots.length);
    this.selectPosition(this.selectedPosition);
    // this.remainingSlots = this.slots.length - playerCount;
  }


}