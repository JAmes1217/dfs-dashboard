import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from 'app/services/analytics.service';
import { LeagueService } from 'app/services/league.service';
import { Observable } from 'rxjs/Observable';
import { League } from 'app/model/league.model';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {
  public currentView: string = 'rules';
  public leagues: Observable<League []>;
  public secondInputValue: any = 1;
  constructor(
    private analyticsService: AnalyticsService,
    private leagueService: LeagueService
  ) {
    this.leagues = this.leagueService.leagueList();
  }

  ngOnInit() {
    this.leagueService.loadAllLeagues();
    this.leagueService.leagueList().subscribe(res => {
    });
   }

  changeCurrentActivity() {
    if(this.currentView == 'view_prize') {
      this.analyticsService.fetchDailyActivities(true, 'action', 'view-prize-modal');
    } else if(this.currentView == 'rules') {
      if(this.secondInputValue && this.secondInputValue != 1) {
        this.analyticsService.fetchDailyActivities(true, 'visit-rules-page', this.secondInputValue);
      } else {
        this.secondInputValue = 1;
        this.analyticsService.fetchDailyActivities(true);
      }
    } else if(this.currentView == 'contest_info' && this.secondInputValue) {
      this.analyticsService.fetchDailyActivities(true, 'view-contest', this.secondInputValue);
    } else if(this.currentView == 'draft_team') {
      this.analyticsService.fetchDailyActivities(true, 'action','submit-entry');
    }
  }
}