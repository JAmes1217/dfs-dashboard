import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from 'app/services/analytics.service';

@Component({
  selector: 'app-chart-daily-device',
  template: `
  <div><i *ngIf="loading" class="fa fa-spin fa-spinner fa-fw"></i>Last update @ {{ lastUpdate | date: 'yyyy/MM/dd H:mm:ss' }}</div>
  <ngx-charts-bar-vertical-stacked
  [view]="view"
  [scheme]="colorScheme"
  [(results)]="multi"
  [gradient]="gradient"
  [xAxis]="showXAxis"
  [yAxis]="showYAxis"
  [legend]="showLegend"
  [showXAxisLabel]="showXAxisLabel"
  [showYAxisLabel]="showYAxisLabel"
  [xAxisLabel]="xAxisLabel"
  [yAxisLabel]="yAxisLabel"
  (select)="onSelect($event)">
</ngx-charts-bar-vertical-stacked>
`,

})
export class ChartDailyDeviceComponent implements OnInit {
  multi: any[];
  view: any[] = [900, 300];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Date';
  showYAxisLabel = true;
  yAxisLabel = 'Total Count';
  lastUpdate;
  loading: boolean;
  display: boolean;
  colorScheme = {
    domain: [ '#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };
  constructor(
    private analyticsService: AnalyticsService
  ) {
    
    this.analyticsService.subscribeDailyDeviceShare().subscribe(result => {
      this.multi = result;
      this.display = true;
      this.lastUpdate = Date.now();
      this.loading = false;
    });

   }

  ngOnInit() { 
    this.loading = true;
    this.analyticsService.fetchDailyDeviceShare();

    setTimeout(() => {
      this.loading = true;
      this.analyticsService.fetchDailyDeviceShare();
    }, 10000)
  }

  onSelect(event) {
    console.log(event);
  }
}