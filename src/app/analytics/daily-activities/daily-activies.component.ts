import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from 'app/services/analytics.service';

@Component({
    selector: 'app-chart-daily-activities',
    template: `
    <div><i *ngIf="loading" class="fa fa-spin fa-spinner fa-fw"></i>Last update @ {{ lastUpdate | date: 'yyyy/MM/dd H:mm:ss' }}</div>
    <ngx-charts-bar-vertical
      *ngIf="display"
      [view]="view"
      [scheme]="colorScheme"
      [(results)]="single"
      [gradient]="gradient"
      [xAxis]="showXAxis"
      [yAxis]="showYAxis"
      [legend]="showLegend"
      [showXAxisLabel]="showXAxisLabel"
      [showYAxisLabel]="showYAxisLabel"
      [xAxisLabel]="xAxisLabel"
      [yAxisLabel]="yAxisLabel"
      (select)="onSelect($event)">
    </ngx-charts-bar-vertical>
  `
})
export class ChartDailyActivitiesComponent implements OnInit {
    single: any[];
    multi: any[];
    view: any[] = [900, 300];
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    showXAxisLabel = true;
    xAxisLabel = 'Date';
    showYAxisLabel = true;
    yAxisLabel = 'Total Count';
    lastUpdate;
    loading: boolean;
    colorScheme = {
      domain: [ '#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
    };
    public display: boolean = false;
    constructor(private analyticsService: AnalyticsService) { 
      this.loading = false;
      let newDate = new Date();
      
      this.analyticsService.subscribeToDailyActivities().subscribe(results => {
        this.single =  this.analyticsService.getDaysInMonth(newDate.getMonth() + 1 ,newDate.getFullYear());
        if(results) {
          results.map((perRes,i) => {
          let index = this.single.findIndex(x => x.name == perRes['Date']);
          if(index >= 0) {
            this.single[index]['value'] = perRes['totalCount'];
            // console.log(this.single);
          }
          this.display = results.length - 1 == i ? true:false;
          this.lastUpdate = Date.now();
          this.loading = false;
          });
        }
      });
    }

    ngOnInit() { 
      this.loading = true;
      this.analyticsService.fetchDailyActivities(true);

      setTimeout(() => {
        this.loading = true;
        this.analyticsService.fetchDailyActivities(false);
      }, 5000)
    }
    
    onSelect(event) {
      console.log(event);
    }
}