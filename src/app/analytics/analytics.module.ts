import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import {BrowserAnimationsModule} from '@angular/platform-browser-animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AnalyticsRoutingModule } from './analytics-routing.module';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AnalyticsService } from 'app/services/analytics.service';
import { AnalyticsComponent } from 'app/analytics/analytics.component';
import { ChartDailyUserComponent } from 'app/analytics/daily-user/chart.daily-user.component';
import { ChartDailyDeviceComponent } from 'app/analytics/daily-device/chart.daily-device.component';
import { ChartDailyActivitiesComponent } from 'app/analytics/daily-activities/daily-activies.component';
import { ContestAnalyticsComponent } from 'app/analytics/contest-analytics/contest-analytics.component';

@NgModule({
    imports: [AnalyticsRoutingModule, CommonModule, FormsModule, NgxChartsModule],
    exports: [],
    declarations: [ChartDailyUserComponent, AnalyticsComponent, ChartDailyDeviceComponent,ChartDailyActivitiesComponent
    , ContestAnalyticsComponent],
    providers: [AnalyticsService]
})
export class AnalyticsModule {}