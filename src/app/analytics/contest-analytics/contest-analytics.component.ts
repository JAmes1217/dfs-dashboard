import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from 'app/services/analytics.service';

@Component({
  selector: 'app-contest-analytics',
  template: `
    <ngx-charts-line-chart
      [view]="view"
      [scheme]="colorScheme"
      [results]="multi"
      [gradient]="gradient"
      [xAxis]="showXAxis"
      [yAxis]="showYAxis"
      [legend]="showLegend"
      [showXAxisLabel]="showXAxisLabel"
      [showYAxisLabel]="showYAxisLabel"
      [xAxisLabel]="xAxisLabel"
      [yAxisLabel]="yAxisLabel"
      [autoScale]="autoScale"
      (select)="onSelect($event)">
    </ngx-charts-line-chart>
  `
})

export class ContestAnalyticsComponent implements OnInit {
    
    single: any[];
    multi: any[];

    view: any[] = [900, 400];

    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    showXAxisLabel = true;
    xAxisLabel = 'Country';
    showYAxisLabel = true;
    yAxisLabel = 'Population';

    colorScheme = {
        domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    };

    autoScale = true;
  
    constructor(
        private analyticsService: AnalyticsService
    ) {
        this.analyticsService.subscribeToContestAnalytics().subscribe(res => {
            console.log('result', res);
            this.multi = res;
        });
        // this.multi = [
        //     {
        //     "name": "Germany",
        //     "series": [
        //       {
        //         "name": "2010",
        //         "value": 7300000
        //       },
        //       {
        //         "name": "2011",
        //         "value": 8940000
        //       }
        //     ]
        //   },
        
        //   {
        //     "name": "USA",
        //     "series": [
        //       {
        //         "name": "2010",
        //         "value": 7870000
        //       },
        //       {
        //         "name": "2011",
        //         "value": 8270000
        //       }
        //     ]
        //   },
        
        //   {
        //     "name": "France",
        //     "series": [
        //       {
        //         "name": "2010",
        //         "value": 5000002
        //       },
        //       {
        //         "name": "2011",
        //         "value": 5800000
        //       }
        //     ]
        //   }
        // ];
    }
    ngOnInit() {
        this.analyticsService.fetchContestAnalytics();
    }
    onSelect(event) {
        console.log(event);
    }
  
}