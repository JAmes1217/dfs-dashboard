import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AnalyticsComponent } from 'app/analytics/analytics.component';

const routes: Routes = [
    { path: '', component: AnalyticsComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AnalyticsRoutingModule {}
