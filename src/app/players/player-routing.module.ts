import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';
import { PlayerListComponent } from './player-list/player-list.component';
import { EditPlayerComponent } from './player-edit/edit-profile.component';
import { PlayerResolve } from './player.resolver';



const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: PlayerListComponent,
        data: {
          title: 'Player List'
        },
      },
      {
        path: 'edit/:id/league/:leagueId/period/:periodId',
        component: EditPlayerComponent,
        data: {
          title: 'Edit Player'
        },
        resolve : {
          playerInfo: PlayerResolve
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlayerRoutingModule {}
