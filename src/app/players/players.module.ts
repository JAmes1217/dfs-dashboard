import { NgModule } from '@angular/core';

// import { PlayerComponent } from './name.component';
import { PlayerRoutingModule } from './player-routing.module';
import { PlayerListComponent } from './player-list/player-list.component';
import { PlayerSampleComponent } from './player-sample/player-sample.component';
import { EditPlayerComponent } from './player-edit/edit-profile.component';
import { UserService } from '../services/user.service';
import { CommonModule } from '@angular/common';
import { SearchFilterPipe } from '../shared/filter';
import { PlayerService } from '../services/player.service';
import { PlayerResolve } from './player.resolver';
import { LeagueService } from '../services/league.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TeamService } from '../services/team.service';
import { OrderModule } from 'ngx-order-pipe';

@NgModule({
    imports: [PlayerRoutingModule, CommonModule, FormsModule, ReactiveFormsModule,OrderModule],
    exports: [],
    declarations: [PlayerListComponent, PlayerSampleComponent, EditPlayerComponent, SearchFilterPipe],
    providers: [UserService, PlayerService, PlayerResolve, LeagueService, TeamService]
})
export class PlayersModule {

 }
