import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Player } from '../model/player.model';
import { Observable } from 'rxjs/Observable';
import { PlayerService } from '../services/player.service';
import { Injectable } from '@angular/core';


@Injectable()
export class PlayerResolve implements Resolve<Player> {

    constructor(
        private playerService: PlayerService
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Player | Observable<Player> | Promise<Player> {
       return this.playerService.getPlayerById(route.params.id);
    }
}