import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import {  Router, ActivatedRoute, ParamMap } from '@angular/router';
import { LeagueService } from '../../services/league.service';
import { Observable } from 'rxjs/Observable';
import { PlayerPosition } from '../../model/player-position.model';
import { PlayerService } from '../../services/player.service';
import { TeamModel } from '../../model/team.model';
import { TeamService } from '../../services/team.service';
import { Player } from '../../model/player.model';

@Component({
    selector: 'app-game-list',
    templateUrl: 'edit-profile.component.html',
    styleUrls: ['./edit-profile.component.scss']
})

export class EditPlayerComponent implements OnInit {
    public titlegame: string;
    public playerInformation: Player;
    public playerInfoConstant: any;
    public message: any;
    public playerPositions: Observable <PlayerPosition []>;
    public teams: Observable <TeamModel []>;
    public periodKey: any;
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private leagueService: LeagueService,
        private playerService: PlayerService,
        private teamService: TeamService
    ) {
        this.playerInformation = this.route.snapshot.data['playerInfo'];
        this.playerInfoConstant = JSON.stringify(this.playerInformation);
        this.playerPositions = this.leagueService.leaguePlayerPositions();
        this.teams = this.teamService.getAllTeams();
        this.leagueService.getPeriodKey(
            this.route.params['value']['periodId'],
            this.route.params['value']['leagueId'],
            (key) => {
                this.periodKey = key;
            },
            (errMessage) => {
                console.log(errMessage);
            });
        console.log('g', this.periodKey);
    }

    ngOnInit() {
        this.leagueService.loadAllPositionByLeagueId(this.playerInformation['leagueId']);
        this.teamService.loadAllTeams(`/teams?size=9999999&leagueId=${this.playerInformation['leagueId']}`);
    }

    jsonCompare(obj1, obj2) {
        const result = {};
        for (const key in obj1) {
            if (obj2[key] !== obj1[key]) {
                result[key] = obj2[key];
            } else if (obj2[key] instanceof Array &&  obj1[key] instanceof Array) {
                result[key] = arguments.callee(obj1[key], obj2[key]);
            } else if (obj2[key] instanceof Object &&  obj1[key] instanceof Object) {
                result[key] = arguments.callee(obj1[key], obj2[key]);
            }
        }

        return result;
    }

    saveChanges() {
        const getPlayerChanges = this.jsonCompare(JSON.parse(this.playerInfoConstant), this.playerInformation);
        delete getPlayerChanges['averageStats'];
        delete getPlayerChanges['position'];
        const id = this.playerInformation['id'];
        const salary = {};
        salary['amount'] = 0;
        this.playerService.playerUpdateProfileById(salary , getPlayerChanges, id,
            this.playerInformation['leagueId'], this.periodKey ,this.route.params['value']['periodId'], 
            (success) => {
                this.message = success;
            },
            (error) => {
                this.message = error;
            }
        );
    }
    cancelChanges() {
        this.router.navigateByUrl('/players');
    }
}