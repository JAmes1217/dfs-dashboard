import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { PlayerService } from '../../services/player.service';
import { Observable } from 'rxjs/Observable';
import { Player } from '../../model/player.model';
import { LeagueService } from '../../services/league.service';
import { League } from '../../model/league.model';
import { PlayerPosition } from '../../model/player-position.model';
import { Period } from '../../model/period.model';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-player-list',
    templateUrl: 'player-list.component.html',
    styleUrls: ['./player-list.component.scss'],
})

export class PlayerListComponent implements OnInit {
    public name: string;
    public players: Observable<Player []>;
    public leagues: Observable<League []>;
    public playerPositions: Observable<PlayerPosition []>;
    public playerList: Object;
    public page = 1;
    public filterLeagueId: number;
    public filterPositionId: number;
    public filterByName: string;
    public filterByPeriod: string;
    public filterByPlayerName = '';
    public filterPlayerNameControl = new FormControl();
    public orderByName: string = 'name';
    public orderByBool: boolean = true;
    constructor(
     private userService: UserService,
     private router: Router,
     private route: ActivatedRoute,
     private playerService: PlayerService,
     private leagueService: LeagueService
    ) {
        this.players = this.playerService.playerList('players');
        this.leagues = this.leagueService.leagueList();
        this.playerPositions = this.leagueService.leaguePlayerPositions();
        this.filterLeagueId = 0;
        this.filterByPeriod = '';
        this.filterPositionId = 0;
        this.filterByName = '';
        console.log(this.players);
    }

    ngOnInit() {
       this.playerService.loadAllPlayers('/players?size=99999999&available=true&include=team');
       this.leagueService.loadAllLeagues();

       this.filterPlayerNameControl.valueChanges
       .debounceTime(2000)
       .subscribe(newValue => {
            this.filterByPlayerName = newValue
            // console.log('change');
            this.triggerFilterByName();
        });
     }

     viewPlayer(info) {
        let periodId: any;
        if (this.filterByPeriod !== '') {
            periodId = this.leagueService.getPeriodId(this.filterByPeriod, this.filterLeagueId);
            periodId = periodId ? periodId : 0;
        } else {
            periodId = 0;
        }
        this.router.navigateByUrl(`/players/edit/${info.id}/league/${this.filterLeagueId}/period/${periodId}`);
    }

    triggerPage(todo: string) {
        let page = this.page;
        let byLeague = '';

        page = todo === 'add' ? page + 1 : page - 1;
        this.page = page;
        byLeague = this.filterLeagueId > 0 ? `&leagueId=${this.filterLeagueId}` : '';

        this.playerService.loadAllPlayersWithFilterUrl('/players?available=true&include=team&size=30&page=' + page + byLeague);
    }

    triggerFilter() {
        this.filterByPeriod = '';
        this.filterByPlayerName = '';
        this.leagueService.loadAllPositionByLeagueId(this.filterLeagueId);
        this.playerService.loadAllPlayersWithFilterUrl(`/players?size=9999999&available=true&include=team&leagueId=${this.filterLeagueId}`);
    }

    triggerFilterByName() {
        this.playerService.filterPlayerByName(this.filterByPlayerName, this.filterLeagueId, this.filterByPeriod);
    }

    triggerFilterByPeriod() {
        this.filterByPlayerName = '';
        const periodId = this.leagueService.getPeriodId(this.filterByPeriod, this.filterLeagueId);
        if (periodId) {
            const path = `/players?size=99999999&available=true&include=team&periodId=${periodId}`;
            this.playerService.loadAllPlayersWithFilterUrl(path);
        } else {
            const path = `/players?size=99999999&available=true&include=team&periodId=0`;
            this.playerService.loadAllPlayersWithFilterUrl(path);
        }
    }

    order(to) {
        this.orderByBool = this.orderByName == to && this.orderByBool ? false:true;
        this.orderByName = to;
    }
}