import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Game } from '../../model/game.model';
import { GameService } from '../../services/game.service';
import { Router } from '@angular/router';
import { League } from '../../model/league.model';
import { LeagueService } from '../../services/league.service';

@Component({
    selector: 'app-game-list',
    templateUrl: 'game-list.components.html',
    styleUrls: ['./game-list.components.scss']
})

export class GameListComponent implements OnInit {
    public games: Observable<Game []>;
    public leagues: Observable<League []>;
    public page: number;
    public filterLeagueId: number;
    public filterPeriod: string;
    public periodId: string;


    constructor(
        private gameService: GameService,
        private leagueService: LeagueService,
        private router: Router
    ) {
        this.games = this.gameService.gameList();
        this.leagues = this.leagueService.leagueList();
        this.page = 1;
        this.filterLeagueId = 0;
        this.filterPeriod = 'Current';
    }

    ngOnInit() {
        this.filterPeriod = 'Current';
        this.leagueService.loadAllLeagues();
        this.gameService.loadAllComingGames();
        
    }

    viewGame(id: number) {
        this.router.navigateByUrl(`games/edit/${id}`);
    }

    triggerPage(todo: string) {
        let page = this.page;
        // let byLeague = '';

        page = todo === 'add' ? page + 1 : page - 1;
        this.page = page;
        this.gameService.loadAllGames('/games?size=30&page=' + page);
    }

    triggerFilter() {
        let path = '';
        const periodId = this.leagueService.getPeriodId(this.filterPeriod, this.filterLeagueId);
        if (periodId) {
            this.gameService.gamesByPeriodId(periodId);
        } else {
            alert('NO PERIOD ID');
        }
    }
}