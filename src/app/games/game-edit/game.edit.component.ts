import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GameService } from '../../services/game.service';




@Component({
    selector: 'app-game-edit',
    templateUrl: 'game-edit.component.html',
    styleUrls: ['./game-edit.component.scss']

})
export class GameEditComponent implements OnInit {
    public gameInfo= this.route.snapshot.data['gameInfo'];
    public gameInfoConstantValue: any;
    public message: string;
    constructor(
        private router: Router,
        private gameService: GameService,
        private route: ActivatedRoute
    ) {
        this.gameInfoConstantValue = JSON.stringify(this.route.snapshot.data['gameInfo']);
    }

    ngOnInit() {
        console.log('info', this.gameInfo);
    }

    compareJson(obj1, obj2) {
        const result = {};
        for (const key in obj1) {
            if (obj2[key] !== obj1[key]) {
                result[key] = obj2[key];
            } else if (obj2[key] instanceof Array &&  obj1[key] instanceof Array) {
                result[key] = arguments.callee(obj1[key], obj2[key]);
            } else if (obj2[key] instanceof Object &&  obj1[key] instanceof Object) {
                result[key] = arguments.callee(obj1[key], obj2[key]);
            }
        }

        return result;
    }

    saveChanges() {
        const compareData = this.compareJson(JSON.parse(this.gameInfoConstantValue), this.gameInfo);
        delete compareData['team1'];
        delete compareData['team2'];
        this.gameService.updateGame(compareData, this.gameInfo.id).subscribe((res) => {
            this.message = 'Game Details was successfully updated.';
        },
        (err) => {
            this.message = 'Error Updating Game Detail.';
        });
    }

    cancelChanges() {
        this.router.navigateByUrl('/games');
    }
}