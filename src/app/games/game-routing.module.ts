import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';
import { GameListComponent } from './game-list/game-list.components';
import { GameEditComponent } from './game-edit/game.edit.component';
import { GameResolve } from './game.resolve';



const routes: Routes = [
  {
    path: '',
    component: GameListComponent,
    data: {
      title: 'Game List'
    }
  },
  {
    path: 'edit/:id',
    component: GameEditComponent,
    data: {
      title: 'Game Edit'
    },
    resolve: {
      gameInfo: GameResolve
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule {}
