import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Game } from '../model/game.model';
import { GameService } from '../services/game.service';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class GameResolve implements Resolve<Game> {

    constructor(
        private gameService: GameService,
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Game | Observable <Game> | Promise<Game> {
        return this.gameService.getGameById(route.params.id);
    }
}