import { NgModule } from '@angular/core';

// import { PlayerComponent } from './name.component';
import { GameRoutingModule } from './game-routing.module';
import { GameListComponent } from './game-list/game-list.components';
import { UserService } from '../services/user.service';
import { GameService } from '../services/game.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { GameEditComponent } from './game-edit/game.edit.component';
import { GameResolve } from './game.resolve';

@NgModule({
    imports: [GameRoutingModule, CommonModule, FormsModule],
    exports: [],
    declarations: [GameListComponent, GameEditComponent],
    providers: [UserService, GameService, GameResolve]
})
export class GameModule {

 }
