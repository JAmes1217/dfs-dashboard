import { OnInit, Component } from '@angular/core';
import { Route, Router } from '@angular/router';
import { TeamService } from '../../services/team.service';
import { TeamModel } from '../../model/team.model';
import { Observable } from 'rxjs/Observable';
import { LeagueService } from '../../services/league.service';
import { League } from '../../model/league.model';


@Component({
    selector: 'app-team-list',
    templateUrl: 'team-list.component.html',
    styleUrls: ['./team-list.component.scss']
})
export class TeamListComponent implements OnInit {
    public teams: Observable<TeamModel []>;
    public leagues: Observable<League []>;
    public filterLeagueId: number;
    constructor(
        private router: Router,
        private teamService: TeamService,
        private leagueService: LeagueService
    ) {
        this.teams = this.teamService.getAllTeams();
        this.leagues = this.leagueService.leagueList();
        this.filterLeagueId = 0;
    }

    ngOnInit() {
        this.teamService.loadAllTeams('/teams?size=999999');
        this.leagueService.loadAllLeagues();
    }

    viewTeam(data: any) {
        // console.log(data);
        this.router.navigateByUrl(`/teams/edit/${data.id}`);
    }
    triggerFilter(by: string) {
        let path = '';
        path = `/teams?leagueId=${this.filterLeagueId}&size=999999`;
        this.teamService.loadAllTeams(path);
    }
}