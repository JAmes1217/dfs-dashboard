import { Injectable } from '@angular/core';
import { TeamService } from '../services/team.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TeamModel } from '../model/team.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TeamResolve {
    constructor(
        private teamService: TeamService
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): TeamModel | Observable<TeamModel> | Promise <TeamModel> {
        return this.teamService.getTeamById(route.params.id);
    }
}