import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TeamService } from '../../services/team.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/throttleTime';
import 'rxjs/add/observable/fromEvent';
import { TeamModel } from 'app/model/team.model';


@Component({
    selector: 'app-team-edit',
    templateUrl: 'team-edit.component.html',
    styleUrls: ['./team-edit.component.scss']
})

export class TeamEditComponent implements OnInit {
    // firstName        = 'Name';
    // firstNameControl = new FormControl();
    public teamInfo: TeamModel;
    public constantTeamInfo: any;
    public message: string;
    constructor(
        public router: Router,
        public teamService: TeamService,
        public route: ActivatedRoute
    ) {
       this.teamInfo = this.route.snapshot.data['teamInfo'];
       this.constantTeamInfo = JSON.stringify(this.route.snapshot.data['teamInfo']);
       this.message = '';
    }

    ngOnInit() {
    }

    jsonCompare(obj1, obj2) {
        const result = {};
        for (const key in obj1) {
            if (obj2[key] !== obj1[key]) {
                result[key] = obj2[key];
            } else if (obj2[key] instanceof Array &&  obj1[key] instanceof Array) {
                result[key] = arguments.callee(obj1[key], obj2[key]);
            } else if (obj2[key] instanceof Object &&  obj1[key] instanceof Object) {
                result[key] = arguments.callee(obj1[key], obj2[key]);
            }
        }
        return result;
    }

    saveChanges() {
        const getChanges = this.jsonCompare(JSON.parse(this.constantTeamInfo), this.teamInfo);
        const path = `/teams/${this.teamInfo['id']}`;
        this.teamService.saveChanges(path, getChanges,
            (successMessage) => {
                this.message = successMessage;
            },
            (errorMessage) => {
                this.message = errorMessage;
            }
        );
    }
    cancelChanges() {
        this.router.navigateByUrl('/players');
    }
}