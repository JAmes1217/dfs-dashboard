import { NgModule } from '@angular/core';
import { TeamListComponent } from './team-list/team-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TeamRoutingModule } from './team-routing.module';
import { TeamEditComponent } from './team-edit/team-edit.component';
import { TeamService } from '../services/team.service';
import { TeamResolve } from './team.resolver';

@NgModule({
    imports: [TeamRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports: [],
    declarations: [TeamListComponent, TeamEditComponent],
    providers: [TeamService, TeamResolve]
})
export class TeamsModule {
}