import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamEditComponent } from './team-edit/team-edit.component';
import { TeamResolve } from './team.resolver';



const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: TeamListComponent,
        data: {
          title: 'Team List'
        },
      },
      {
        path: 'edit/:id',
        component: TeamEditComponent,
        data: {
          title: 'Team Edit'
        },
        resolve: {
          teamInfo: TeamResolve
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule {}
