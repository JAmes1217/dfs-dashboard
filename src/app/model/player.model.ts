export class Player {
    name: string
    id: number
    salary: number
    jerseyNumber: number
    positionId: number 
    teamId: number 
    imageUrl: string
    position: any
}