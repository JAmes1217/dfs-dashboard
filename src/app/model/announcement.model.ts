export class Announcement {
  language: string
  message: string
  reference: string
  publish: boolean
}