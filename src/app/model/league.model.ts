export class League {
    id: number
    name: string
    nextPeriodId: number
    settings: {
        abbreviation: string,
        name: string,
        lineupSlots: any,
        positions: any
    }
    currentPeriodId: number
    previousPeriodId: number
}