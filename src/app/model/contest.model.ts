export class Contest {
    id: number
    name: string
    tournament: boolean
    entryFee: number
    leagueId: number
    maxEntries: number
    minEntries: number
    maxEntriesPerPlayer: number
    minPrizePool: number
    prizeStructure: string
    prizes: { fromRank: number, toRank: number, shareOfPrizePool: number } []
    periodId: number;
    maxPrizeTable: { fromRank: number, toRank: number, shareOfPrizePool: number } []
    maxPrizePool: number
    numberOfEntries: number
    privateContest: boolean
}