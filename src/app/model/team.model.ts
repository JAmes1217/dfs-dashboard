export class TeamModel {
    id: number
    name: string
    imageUrl: string
    leagueId: number
    profileUrl: string;
    thumbnailUrl: string;
    abbreviation: string;
}