export class PlayerPosition {
    id: number
    code: string
    name: string
    description: string
}