import { Component, OnInit } from '@angular/core';
import { AnnoucementService } from '../../services/announcement.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { Announcement } from '../../model/announcement.model';

@Component({
    selector: 'app-create-announcement',
    templateUrl: './create-announcement.component.html',
    styleUrls: ['./create-announcement.component.scss']
})
export class CreateAnnouncementComponent implements OnInit {
    public announcementStatus: boolean = false;
    public displayCreateMessage: boolean = false;
    public returnMessage: string;
    public message: Announcement = 
        {
            language: 'en',
            message: '',
            reference: environment.baseUrl ,
            publish: false
        };
    public languageList: Object[];
    public language: string = 'en';
    constructor(
        private announcementService: AnnoucementService,
        private router: Router
    ) { }

    ngOnInit() {
        this.languageList = [
            {value:'en', displayText: 'ENGLISH'},
            {value:'cn', displayText: 'CHINESE'},
        ];
        this.removeLanguageExistInMessage();
    }


    setDisplayCreateMessage(status: boolean) {
        this.displayCreateMessage = status;

    }

    removeLanguageExistInMessage() {
        let index = this.languageList.findIndex(lang => lang['value'] == this.language);
        if(index >= 0) {
            this.languageList.splice(index, 1);
        }
        this.language = '';
    }

    submit() {
        this.announcementService.submitAnnouncement(this.message).subscribe(result => {
            this.returnMessage = result.json().message;
            setTimeout(() => {
                this.router.navigateByUrl('/announcement');
            }, 1500);
        }, error => {
            this.returnMessage = 'error';
        });
    }

    cancelChanges() {
        this.router.navigateByUrl('/announcement');
    }

}