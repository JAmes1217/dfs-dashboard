import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AnnouncementListComponent } from './list/announcement-list.component';
import { CreateAnnouncementComponent } from './create/create-announcement.component';
import { EditAnnouncementComponent } from './edit/edit-announcement.component';


const routes: Routes = [
    { path: '', component: AnnouncementListComponent },
    { path: 'create', component: CreateAnnouncementComponent },
    { path: 'edit', component: EditAnnouncementComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AnnouncementRoutingModule {}
