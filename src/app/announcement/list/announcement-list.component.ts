import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AnnoucementService } from '../../services/announcement.service';

@Component({
    selector: 'app-announcement-list',
    templateUrl: './announcement-list.component.html',
    styleUrls: ['./announcement-list.component.scss']
})
export class AnnouncementListComponent implements OnInit {
    public announcementList: Observable<Object []>;
    constructor
    (
        private announcementService: AnnoucementService
    ) { 
        this.announcementList = this.announcementService.subscribeAnnouncement();

    }

    ngOnInit() { 
        this.announcementService.fetchAnnouncement();
    }

    deleteAnnouncement(id) {
        confirm('Are you sure to delete '+id +' ?');
    }
}