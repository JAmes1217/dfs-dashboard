import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AnnouncementRoutingModule } from './announcement-routing.module';
import { AnnoucementService } from '../services/announcement.service';
import { AnnouncementListComponent } from './list/announcement-list.component';
import { CreateAnnouncementComponent } from './create/create-announcement.component';
import { EditAnnouncementComponent } from './edit/edit-announcement.component';

@NgModule({
    declarations: [AnnouncementListComponent, CreateAnnouncementComponent, EditAnnouncementComponent],
    imports: [ CommonModule, FormsModule, AnnouncementRoutingModule],
    exports: [],
    providers: [AnnoucementService],
})
export class AnnouncementModule {}