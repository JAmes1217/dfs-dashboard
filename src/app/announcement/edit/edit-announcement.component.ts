import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-edit-announcement',
    templateUrl: './edit-announcement.component.html',
    styleUrls: ['./edit-announcement.component.scss']
})
export class EditAnnouncementComponent implements OnInit {
    public announcementStatus: boolean = false;
    public displayCreateMessage: boolean = false;
    public returnMessage: string;
    public message;
    constructor() { }

    ngOnInit() { }

    saveChanges() {

    }

    cancelChanges() {

    }

}