import { NgModule, ModuleWithProviders } from '@angular/core';

// import { PlayerComponent } from './name.component';
import { UserService } from '../services/user.service';
import { CommonModule } from '@angular/common';
import { PlayerService } from './player.service';
import { LeagueService } from './league.service';
import { AuthService } from './auth.service';
import { AuthGuard } from './guard.service';
import { TeamService } from './team.service';
import { AnalyticsService } from 'app/services/analytics.service';
import { AnnoucementService } from 'app/services/announcement.service';

@NgModule({
    imports: [CommonModule],
    exports: [],
    declarations: []
})
export class ServiceModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ServiceModule,
            providers: [
                UserService,
                PlayerService,
                LeagueService,
                TeamService,
                AuthService,
                AuthGuard,
                AnalyticsService,
                AnnoucementService
            ]
        }
    }
 }
