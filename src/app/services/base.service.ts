import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

export class BaseService {
    base_url = environment.baseUrl;
    constructor(public http: Http) {
    }

    public getHeaders() {
        const headers = new Headers();

       headers.append('Content-Type', 'application/json');
        if (localStorage.getItem('auth_token')) {
            headers.append('Authorization', 'token ' + localStorage.getItem('auth_token'));
        }

        return headers;
    }

    public $get(url: string) {
        return this.http.get(this.base_url + url, {headers: this.getHeaders()})
    }

    public $post(url: string, data: any) {
        return this.http.post(this.base_url + url, JSON.stringify(data), {headers: this.getHeaders()});
    }

    public $put(url: string, data: any) {
        return this.http.put(this.base_url + url, JSON.stringify(data), {headers: this.getHeaders()});
    }
}