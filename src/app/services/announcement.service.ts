import { Injectable } from '@angular/core';
import { BaseService } from 'app/services/base.service';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';

@Injectable()
export class AnnoucementService {
    private announcementList: BehaviorSubject<any>;
    constructor(
        private http: Http
    ) {
        this.announcementList = <BehaviorSubject <Object []> > new BehaviorSubject([]);
    }
    
    fetchAnnouncement() {
        let url = `https://analytics.super7.ph/announcement?reference=${ environment.baseUrl }&public=true`;
        this.http.get(url).subscribe(res => {
            this.announcementList.next(res.json());
        });
    }

    subscribeAnnouncement(): Observable<Object []>  {
        return this.announcementList.asObservable();
    }

    submitAnnouncement(message: Object) {
     return this.http.post("https://analytics.super7.ph/announcement", message);
    }
}