import { BaseService } from './base.service';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { League } from '../model/league.model';
import { Observable } from 'rxjs/Observable';
import { PlayerPosition } from '../model/player-position.model';
import { Period } from '../model/period.model';

@Injectable()
//
export class LeagueService extends BaseService {
    public leagues: BehaviorSubject <League []>;
    public playerPositions: BehaviorSubject <PlayerPosition []>
    constructor(
        http: Http,
        private route: Router
    ) {
        super(http);
        this.leagues = <BehaviorSubject < League []> > new BehaviorSubject([]);
        this.playerPositions = <BehaviorSubject < PlayerPosition [] > > new BehaviorSubject([]);
    }

    loadAllLeagues() {
        let path = '/leagues?include=settings&size=99999';
        this.$get(path).map(res => res.json() as League [])
        .subscribe(league => {
            // console.log('im your league', league);
            this.leagues.next(league['results'].map(function(content) {
                return content
            }));
          });
    }

    loadAllPositionByLeagueId(id: number) {
        this.$get(`/leagues/${id}?include=settings`).map(res => res.json())
        .subscribe(leagueInfo => {
            this.playerPositions.next(leagueInfo['settings']['positions'].map(function(position){
                return position
            }))
        });
    }

    leagueList() {
        return this.leagues.asObservable();
    }

    leaguePlayerPositions() {
        return this.playerPositions.asObservable();
    }

    getLeagueByLeagueId(id: number) {
        return this.$get(`/leagues/${id}?include=settings`).map(res => res.json() as League);
    }

    getPeriodId(period: string, id: number) {
        const leagueInfo = this.leagues.value.filter(league => league.id == id);
      
        switch (period) {
            case 'Current':
                return leagueInfo[0]['currentPeriodId'];
            case 'Next': 
                return leagueInfo[0]['nextPeriodId'] ;
            case 'Prev': 
                return leagueInfo[0]['previousPeriodId'];

        
            default:
                return leagueInfo[0]['currentPeriodId'];
        }
    }

    getPeriodKey(periodId: number, leagueId: number, successCallback: (param: any) => any, errorCallback: (param: any) => any) {
        return this.$get('/leagues?include=settings&size=99999').map(res => res.json())
        .subscribe(
            leagues => {
                const list = leagues['results'].filter(league => league.id == leagueId);
                let key = '';
                console.log(list);
                if (list) {
                    key = list['0']['currentPeriodId'] == periodId ? 'Current' :
                        list['0']['nextPeriodId'] == periodId ? 'Next' :
                        list['0']['previousPeriodId'] == periodId ? 'Previous': 'Not Found';

                    successCallback(key)
                }
          },
          err => {
              errorCallback('Error Please Try again');
          }
        );
    }
}