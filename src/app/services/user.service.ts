import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { BaseService } from 'app/services/base.service';

@Injectable()
export class UserService extends BaseService {
    public loginData = Object;
    public active = false;

    constructor(http: Http,
        private router: Router
    ) {
        super(http);
     }

    getUser(): string {
        return 'james';
    }

    getUsername(): string {
        return 'ichie';
    }

    submitLog(content: Object) {
        if (content['username'] === 'James' && content['password'] === '123') {
           this.active = true;
           this.router.navigateByUrl('/dashboard');
        } else {
            return '<span> Error username and password.</span>';
        }
    }

}
