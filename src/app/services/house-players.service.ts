import { Injectable } from '@angular/core';
import { BaseService } from 'app/services/base.service';
import { Headers, Http } from '@angular/http';
import { Player } from 'app/model/player.model';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class HousePlayersService extends BaseService {
  housePlayers = environment.housePlayers;
  password = environment.housePlayersPassword;
  currentLoginAuthToken: string;
  finishedSubmission: BehaviorSubject<number>;

  constructor(
    http: Http
  ) {
    super(http);
    this.currentLoginAuthToken = "";
    this.finishedSubmission = <BehaviorSubject<number>> new BehaviorSubject(0);
  }

  join (contestId, housePlayers: Array<string>, draftedPlayers: Array<any>, playerPool: Player []) {
    this.finishedSubmission.next(0);
    housePlayers.forEach((housePlayerIndex, index) => {

      const hpIndex = Number(housePlayerIndex) -1;
        this.loginHousePlayer(this.housePlayers[hpIndex], this.password)
          .subscribe(auth => {
            this.currentLoginAuthToken = auth['accessToken'];
            console.log(playerPool);
            const newEntry = this.createRandomizedEntry(draftedPlayers, playerPool);
            this.sumbitEntry(contestId, newEntry, auth['accessToken']);
          });
      // },
        // 1000 * index);
     
    })
  }

  subscribeFinishedSubmission() :Observable<number> {
   return this.finishedSubmission.asObservable();
  }

  private sumbitEntry(contestId: number, players, accessToken) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'token ' + accessToken);

    const data = {
      players: this.buildLineup(players),
      contestId: contestId
    };

    this.http.post(environment.baseUrl + `/entries`, data, {headers})
        .subscribe(
          (res) => {
          let finished = this.finishedSubmission.value + 1;
          this.finishedSubmission.next(finished);
          },
          (err) => {
            let finished = this.finishedSubmission.value + 1;
            this.finishedSubmission.next(finished);
          }
        )
  }

  private buildLineup(playerList) {
    let storage = [];
    playerList.map(player => {
      storage.push({
        slotId: player.slot.id,
        playerId: player.player.id
      });
    });

    return storage;
  }

  private createRandomizedEntry(draftedPlayers: Array<any>, playerPool: Player []) {
    let remainingSalaryCap = 50000;

    draftedPlayers.forEach(item => {
      remainingSalaryCap -= item['player']['salary'];
    });

    console.log(draftedPlayers);

    const totalSlots = draftedPlayers.length - 1;
    const randomNum =  Math.floor(Math.random() * totalSlots) + 0;  

    const playerToBeRemoved = draftedPlayers[randomNum]['player'];
    remainingSalaryCap = remainingSalaryCap +  playerToBeRemoved['salary'];
    const playerToBeAdded = this.getQualifiedSportPlayer(remainingSalaryCap, playerPool, draftedPlayers[randomNum]['slot']);

    draftedPlayers[randomNum]['player'] = playerToBeAdded;

    return draftedPlayers;
    

  }

  private getQualifiedSportPlayer(remainingSalary: number, playerPool: Array<any>, slot) {
    let playerQualified: boolean = false;
    do {
      
      let pl = playerPool.filter(item => Number(item.salary) <= Number(remainingSalary));
      console.log(pl);
      let randomNum = Math.floor(Math.random() * pl.length) + 0;
      console.log(randomNum);
      let playerToBeAdded = pl[randomNum];
      if (this.isPlayerFitInSlot(playerToBeAdded, slot)) {
        return playerToBeAdded;
      }

    } while(!playerQualified);
    
  
  }

  private isPlayerFitInSlot(player, slot) {

      for (let ii = 0; ii < slot['actualPositions'].length; ii++) {
        let actualPosition = slot.actualPositions[ii];
        if (actualPosition.id == player['positionId']) { return true; }
      }

    
    return false;
  }

  private loginHousePlayer(username, password) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    return this.http
      .post(environment.baseUrl + '/sessions', JSON.stringify({ username: username, password: password }), { headers })
      .map((res) => res.json());


  }

}