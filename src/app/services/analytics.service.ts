import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AnalyticsService {
    private base_url: string = 'https://analytics.super7.ph';
    private dailyActiveUser: BehaviorSubject<any []>;
    private dailyDeviceShare: BehaviorSubject<any []>;
    private dailyActivities: BehaviorSubject<any []>;
    private contestAnalytics: BehaviorSubject<any []>;
    private dailyActionKey: string;
    private dailyActionValue: any;
    constructor(public http: Http) {
        this.dailyActiveUser = <BehaviorSubject <any []>> new BehaviorSubject([]);
        this.dailyDeviceShare = <BehaviorSubject <any []>> new BehaviorSubject([]);
        this.dailyActivities = <BehaviorSubject <any []>> new BehaviorSubject([]);
        this.contestAnalytics = <BehaviorSubject <any []>> new BehaviorSubject([]);
        this.contestAnalytics.next(
            [ 
             {    name: 'entered a contest', series: [] },
             {    name: 'drafted a player', series: [] },
             {    name: 'submitted an entry', series: [] }
            ]
     );
    }

    private $get(url: string) {
        return this.http.get(this.base_url + url);
    }

    get(url: string) {
        return this.$get(url);
    }

    fetchDailyActivities(save: boolean,key: string = 'rules-page', value: any = 1) {
        if(save) {
            this.saveDailyActionsFilter(false, key,value);
        }
        this.$get(`/data/daily/${key}?value=${value}&source=dfstars.com`)
        .subscribe(res => {
            this.dailyActivities.next(res.json()['result']);
        }, error => {
            this.dailyActivities.next([]);
        })
    }

    subscribeToDailyActivities() {
        return this.dailyActivities.asObservable();
    }

    subscribeToContestAnalytics() {
        return this.contestAnalytics.asObservable();
    }
    fetchDailyActiveUsers () {
        this.$get('/data/daily/active_user?value=1&source=dfstars.com')
            .map(res => res.json()['result'])
            .subscribe(results => {
                this.dailyActiveUser.next(results);
            });
    }

    subscribeDailyActiveUser() {
        return this.dailyActiveUser.asObservable();
    }

    fetchDailyDeviceShare() {
        let newDate = new Date();
        let dayThisMonth = this.setupGroupedDataSetForDayInMonth(newDate.getMonth() + 1 ,newDate.getFullYear());
        

        this.$get('/data/daily/device?value=mobile&source=dfstars.com')
            .map(res => res.json()['result'])
            .subscribe(mobileResult => {
                this.$get('/data/daily/device?value=desktop&source=dfstars.com')
                    .map(res => res.json()['result'])
                    .subscribe(desktopResult => {
                        mobileResult.forEach(daily => {
                            let index = dayThisMonth.findIndex(x => x.name == daily['Date']);
                            if(index >= 0 && dayThisMonth[index]) {
                                
                                    dayThisMonth[index]['series'].push({
                                        name: "Mobile",
                                        value: daily['totalCount']
                                    });
                                
                               
                            }
                            this.dailyDeviceShare.next(dayThisMonth);
                        });

                        desktopResult.forEach(daily => {
                            let index = dayThisMonth.findIndex(x => x.name == daily['Date']);
                            if(index >= 0 && dayThisMonth[index]) {
                                dayThisMonth[index]['series'].push({
                                    name: "Desktop",
                                    value: daily['totalCount']
                                });
                            }
                            this.dailyDeviceShare.next(dayThisMonth);
                        });
                        
                     
                    });    
            });
    }

    subscribeDailyDeviceShare() {
        return this.dailyDeviceShare.asObservable();
    }

    setupGroupedDataSetForDayInMonth(month, year) {
        month = month - 1;
        // Since no month has fewer than 28 days
        var date = new Date(year, month, 1);
        var days = [];
        while (date.getMonth() === month) {
           //  console.log(date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate());
           // days.push(new Date(date));
           let newMonth, newDate;
           newMonth = (date.getMonth() + 1 ) < 10 ? "0" + (date.getMonth() + 1 ): (date.getMonth() + 1);
           newDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
           let data = {
               name: date.getFullYear() + '-' + newMonth +'-' + newDate,
               series: []
           }
           days.push(data);
           date.setDate(date.getDate() + 1);
        }
        return days;
    }
    fetchContestAnalytics() {
        // let newDate = new Date();
        // let dayThisMonth = this.setupGroupedDataSetForDayInMonth(newDate.getMonth() + 1 ,newDate.getFullYear());
        
        let list = this.contestAnalytics.value;
        this.$get(`/data/daily/action?value=enter-contest&source=dfstars.com`)
        .map(res => res.json()['result'])
        .subscribe(result => {
            let storage = [];
            const res = result.map(perRes => {
                storage.push({
                    name: perRes['Date'],
                    value: perRes['totalCount']
                });
            });
            // console.log('enter contest',JSON.stringify(storage));
            // let list = this.contestAnalytics.value;
            // // console.log(this.contestAnalytics.value[0]['series']);
            // let list= this.contestAnalytics.value.concat({
            //     name: 'entered a contest',
            //     series: storage
            // });
            list[0].series = storage;
            // console.log(list);
            this.contestAnalytics.next(list);
        });

        this.$get(`/data/daily/action?value=drafted-a-player&source=dfstars.com`)
        .map(res => res.json()['result'])
        .subscribe(result => {
            let storage = [];
            const res = result.map(perRes => {
                storage.push({
                    name: perRes['Date'],
                    value: perRes['totalCount']
                });
            });
            // console.log('draft player', storage);
            // let list = this.contestAnalytics.value;
            // let list= this.contestAnalytics.value.concat({
            //     name: 'drafted a player',
            //     series: storage
            // });
            // let list = this.contestAnalytics.value[1]['series'] = storage;
            list[1].series = storage;
            // console.log(list);
            this.contestAnalytics.next(list);
        });
    
        this.$get(`/data/daily/action?value=submit-entry&source=dfstars.com`)
        .map(res => res.json()['result'])
        .subscribe(result => {
            let storage = [];
            const res = result.map(perRes => {
                storage.push({
                    name: perRes['Date'],
                    value: perRes['totalCount']
                });
            });
            // console.log('submit-entry',storage);
            // let list = this.contestAnalytics.value;
            let list= this.contestAnalytics.value.concat({
                name: 'submitted an entry',
                series: storage
            });
            // list[2]['series'] = storage;
            // console.log(list);
            this.contestAnalytics.next(list);
        });
    }

    getDaysInMonth(month, year) {
    	month = month - 1;
         // Since no month has fewer than 28 days
         var date = new Date(year, month, 1);
         var days = [];
         while (date.getMonth() === month) {
            //  console.log(date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate());
            // days.push(new Date(date));
            let newMonth, newDate;
            newMonth = (date.getMonth() + 1 ) < 10 ? "0" + (date.getMonth() + 1 ): (date.getMonth() + 1);
            newDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            let data = {
                name: date.getFullYear() + '-' + newMonth +'-' + newDate,
                value: 0
            }
            days.push(data);
            date.setDate(date.getDate() + 1);
         }
         return days;
    }

    saveDailyActionsFilter(reloadData,key: string ='', value: any = '') {
        if(!reloadData) {
            this.dailyActionKey = key;
            this.dailyActionValue = value;
        } else {
            this.fetchDailyActivities(false, this.dailyActionKey,this.dailyActionValue);
        }
    }
}