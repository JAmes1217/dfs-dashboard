import { BaseService } from './base.service';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { TeamModel } from '../model/team.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class TeamService extends BaseService {
    public teams: BehaviorSubject <TeamModel []>;
    constructor(
        http: Http,
        private router: Router
    ) {
        super(http);
        this.teams = <BehaviorSubject < TeamModel [] > > new BehaviorSubject([]);
    }

    closeLoadingDial() {
        document.getElementById('teams-table-loader').style.display = 'none';
        document.getElementById('teams-table-empty').style.display = 'none';
        document.getElementById('teams-table-content').style.display = '';
    }

    hideTableContent() {
        document.getElementById('teams-table-content').style.display = 'none';
        document.getElementById('teams-table-empty').style.display = 'none';
        document.getElementById('teams-table-loader').style.display = '';
    }

    showEmptyContent() {
        document.getElementById('teams-table-content').style.display = 'none';
        document.getElementById('teams-table-empty').style.display = '';
        document.getElementById('teams-table-loader').style.display = 'none';
    }

    loadAllTeams(path: string) {
        this.hideTableContent();
        this.$get(path).map(res => res.json() as TeamModel [])
        .subscribe(teamResult => {
            this.teams.next(teamResult['results'].map(team => {
                return team;
            }));
            this.closeLoadingDial();
                if (teamResult['results'].length == 0) {
                    this.showEmptyContent();
                }
          });
    }

    getAllTeams(): Observable<TeamModel []> {
        return this.teams.asObservable();
    }

    getTeamById(id: number): Observable<TeamModel> {
        return this.$get(`/teams/${id}`).map(res => res.json() as TeamModel);
    }

    updateTeamInfo(path: string, data: any) {
        return  this.$put(path, data).map(res => res.json());
    }

    saveChanges(path: string, data: any, successCallBack, errorCallBack) {
        this.updateTeamInfo(path, data).subscribe((res) => {
            successCallBack('Team Information was successfully updated.');
        },
        (error) => {
            console.log(error);
            errorCallBack('error');
        });
    }
}