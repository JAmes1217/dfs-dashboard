import { BaseService } from './base.service';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/Rx';
import { Contest } from '../model/contest.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LeagueService } from 'app/services/league.service';

@Injectable()
export class ContestService extends BaseService {
    private contest: BehaviorSubject <Contest []>;
    
    constructor(
        http: Http,
        private router: Router,
        private leagueService: LeagueService
    ) {
        super(http);
        this.contest = < BehaviorSubject <Contest []>> new BehaviorSubject([]);
    }
    create(leagueId: number = 0, entryFee:number = 300, maxEntries: number = 2) {
        let contest = {
            leagueId: leagueId,
            entryFee: entryFee,
            maxEntries: maxEntries
        }
        return this.$post('/contests', contest).map((res) => res.json());
    }

    submit(contest) {
        return this.$post(`/contests`, contest).map((res) => res.json());
    }

    closeLoadingDial() {
        document.getElementById('contest-table-loader').style.display = 'none';
        document.getElementById('contest-table-empty').style.display = 'none';
        document.getElementById('contest-table-content').style.display = '';
    }

    hideTableContent() {
        document.getElementById('contest-table-content').style.display = 'none';
        document.getElementById('contest-table-empty').style.display = 'none';
        document.getElementById('contest-table-loader').style.display = '';
    }

    showEmptyContent() {
        document.getElementById('contest-table-content').style.display = 'none';
        document.getElementById('contest-table-empty').style.display = '';
        document.getElementById('contest-table-loader').style.display = 'none';
    }

    loadAllContest(path: string) {
        this.hideTableContent();
        this.$get(path).map(res => res.json() as Contest[])
        .subscribe(contest => {
            this.contest.next(contest['results'].map(function(eachContest){
                return eachContest;
            }));
            this.closeLoadingDial();
                if (contest['results'].length == 0) {
                    this.showEmptyContent();
                }
        });
    }

    fetchContests(leagueId: number, periodId: number = null, page: number = 1) {
        let url = `/contests?size=30&page=${page}`;
        if (leagueId) { url += `&leagueId=${leagueId}`; }
        if (periodId) { url += `&periodId=${periodId}`; }
         

        this.$get(url).map(res => res.json()['results'] as Contest[])
            .subscribe(contests => {
                this.contest.next(contests);
            }); 
    }

    contestList() {
        return this.contest.asObservable();
    }

    subscribeContestList(): Observable<Contest []> {
        return this.contest.asObservable();
    }

    // getAllContestByWithFilter(path: string) {
    //     this.$get(path).map(res => res.json() as Contest[])
    //     .subscribe(contest => {
    //         this.contest.next(contest['results'].map(function(eachContest){
    //             return eachContest;
    //         }));
    //     });
    // }
    getContestById(id: number ): Observable<Contest> {
        return this.$get(`/contests/${id}`).map(res => res.json());
    }

    updateConstest(data: any, id: number) {
        return this.$put(`/contests/${id}`, data).map(res => res.json());
    }
}