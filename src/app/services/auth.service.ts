import { BaseService } from './base.service';
import { Headers, Http } from '@angular/http';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthService extends BaseService {
    public loggedIn = false;
    constructor(
        http: Http,
        private router: Router
    ) {
        super(http);
    }

    login(username, password) {
        const headers = new Headers;
        headers.append('Content-Type', 'application/json');

       return this.http.post( environment.baseUrl + '/sessions', JSON.stringify({username: username, password: password}), {headers})
                .map((res) => res.json())
                .map((res) => {
                    if (res['accessToken']) {
                        localStorage.setItem('auth_token', res['accessToken']);
                        localStorage.setItem('aId', res['userId'])
                        this.loggedIn = true;

                    }

                    return true;
                })
    }

    isLoggedIn() {
        const auth = localStorage.getItem('auth_token');
        return auth;
    }
}