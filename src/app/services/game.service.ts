import { BaseService } from './base.service';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Game } from '../model/game.model';


@Injectable()
export class GameService extends BaseService {
    public games: BehaviorSubject <Game []>;
    constructor(
        http: Http,
        private router: Router
    ) {
        super(http);
        this.games = <BehaviorSubject < Game [] >> new BehaviorSubject([]);
    }

    closeLoadingDial() {
        document.getElementById('games-table-loader').style.display = 'none';
        document.getElementById('games-table-empty').style.display = 'none';
        document.getElementById('games-table-content').style.display = '';
    }

    hideTableContent() {
        document.getElementById('games-table-content').style.display = 'none';
        document.getElementById('games-table-empty').style.display = 'none';
        document.getElementById('games-table-loader').style.display = '';
    }

    showEmptyContent() {
        document.getElementById('games-table-content').style.display = 'none';
        document.getElementById('games-table-empty').style.display = '';
        document.getElementById('games-table-loader').style.display = 'none';
    }

    loadAllGames(path: string) {
        this.$get(path).map(res => res.json() as Game [])
        .subscribe(gameResult => {
            // console.log('im your league', league);
            this.games.next(gameResult['results'].map(function(game) {
                return game
            }));
          });
    }

    gamesByPeriodId(id: number) {
        this.$get(`/games?size=999999&periodId=${id}`).map((result) => result.json())
        .subscribe(gamesResult => {
            this.games.next(gamesResult['results']);
        });
    }
    loadAllComingGames() {
        let storage = [];
        let counter = 0;
        this.hideTableContent();
        this.$get('/leagues').subscribe((res) => {
            res.json().results.map((league) => {
                this.$get(`/games?size=9999999&periodId=${league.currentPeriodId}`).map((result) => result.json())
                .subscribe(gameResult => {
                    storage = storage.concat(gameResult['results']);
                    this.games.next(storage.map(function(game) {
                        return game
                    }));
                    this.closeLoadingDial();

                    if (counter++ == res.json()['results'].length - 1) {
                        if (storage.length == 0) {
                                this.showEmptyContent();
                        }
                    }
                });
            });
        });
    }
    gameList() {
        return this.games.asObservable();
    }

    getGameById(id: number) {
        return this.$get(`/games/${id}`).map(res => res.json() as Game);
    }

    updateGame(data: any, id: number) {
        return  this.$put(`/games/${id}`, data).map(res => res.json());
    }
}