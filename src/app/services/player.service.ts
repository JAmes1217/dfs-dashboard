import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Player } from '../model/player.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base.service';
import { Router } from '@angular/router';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { LeagueService } from './league.service';

@Injectable()
export class PlayerService extends BaseService {
    private players: BehaviorSubject <Player[]>;
    private allPlayers: BehaviorSubject <Player []>;
    private filteredPlayersList: BehaviorSubject <Player []>;
    constructor(http: Http,
        private router: Router,
        private leagueService: LeagueService
    ) {
        super(http);
        this.players = < BehaviorSubject < Player[] >> new BehaviorSubject([]);
        this.allPlayers = < BehaviorSubject < Player[] >> new BehaviorSubject([]);
        this.filteredPlayersList = < BehaviorSubject < Player[] >> new BehaviorSubject([]);
    }

    closeLoadingDial() {
        document.getElementById('table-loader').style.display = 'none';
        document.getElementById('table-empty').style.display = 'none';
        document.getElementById('table-content').style.display = '';
    }

    hideTableContent() {
        document.getElementById('table-content').style.display = 'none';
        document.getElementById('table-empty').style.display = 'none';
        document.getElementById('table-loader').style.display = '';
    }

    showEmptyContent() {
        document.getElementById('table-content').style.display = 'none';
        document.getElementById('table-empty').style.display = '';
        document.getElementById('table-loader').style.display = 'none';
    }

    loadAllPlayers(url: string) {
        this.hideTableContent();
        this.$get(url).map(result => result.json() as Player [])
        .subscribe(players => {
            this.closeLoadingDial();
            this.players.next(players['results'].map(function(player){
                return player
            }))

            this.allPlayers.next(players['results'].map(function(player){
                return player
            }))

            if (players['results'].length === 0) {
                this.showEmptyContent();
            }
        });
    }

    loadAllPlayersWithFilterUrl(url: string) {
        this.hideTableContent();
        this.$get(url).map(result => result.json() as Player [])
        .subscribe(players => {
            this.closeLoadingDial();
            this.players.next(players['results'].map(function(player){
                return player
            }))

            this.filteredPlayersList.next(players['results'].map(function(player){
                return player;
            }))
            if (players['results'].length === 0) {
                this.showEmptyContent();
            }
        });
    }
    playerList(from: string): Observable<Player []> {
        if (from === 'players') {
            return this.players.asObservable();
        } else if (from === 'filterPlayers') {
            return this.filteredPlayersList.asObservable();
        } else {
            return this.allPlayers.asObservable();
        }
    }

    getPlayerById(id: number): Observable<Player> {
        return this.$get(`/players/${id}`).map(res => res.json() as Player);
    }

    updatePlayerInfo(path: string, data: any) {
      return  this.$put(path, data).map(res => res.json());
    }

    filterPlayerByName(name: string, leagueId: number, period: any) {
        let playerList;
        if (leagueId === 0) {
            playerList = this.allPlayers.value;
            this.players.next(playerList.filter(function(player){
                return player.name.toLowerCase().startsWith(name.toLowerCase());
            }));
        } else {
            this.hideTableContent();
            playerList = this.playerList('filterPlayers');
            playerList = playerList.source.value;
            this.players.next(playerList.filter(function(player){
                return player['name'].toLowerCase().startsWith(name.toLowerCase());
            }));
            this.closeLoadingDial();
        }
    }

    playerUpdateProfileById(salary: any , getPlayerChanges: any , playerId: number,
         leagueId: number, key: string , periodId: number,
         successCallback: (param: any) => any, errorCallback: (param: any) => any) {
        if (getPlayerChanges['salary']) {
            salary['amount'] = getPlayerChanges['salary'];
            delete getPlayerChanges['salary'];
        }
        if (JSON.stringify(getPlayerChanges) !== '{}') {
            this.updatePlayerInfo(`/players/${playerId}`, getPlayerChanges).subscribe((res) => {
            successCallback('Player Profile was successfully updated.');
            },
            (err) => {
                errorCallback('Error Updating Player Profile.');
            });
        }
        if (salary['amount'] > 0) {
            if (key === 'Next') {
                this.updatePlayerInfo(`/players/${playerId}/salaries/${periodId}`, salary).subscribe((result) => {
                    successCallback('Player Salary was successfully updated.');
                },
                (error) => {
                    errorCallback('Error Updating Player Salary.');
                });
            } else {
                errorCallback('Failed to update player salary');
            }
        }
    }


    // 

    getPlayersByLeagueId (leagueId) {
        return this.$get(`/players?include=positions,salary,fantasyPointsAverage,team,stats&available=true&size=1000&leagueId=${leagueId}`).map(res => res.json()['results'] as Player[]);
    }
}