import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-login',
    styleUrls: ['./login.component.scss'],
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    public storage = {
        username: '',
        password: ''
    };
    public logStatus = false;
    public message: string;
    constructor(
        private router: Router,
        private userService: UserService,
        private authService: AuthService
    ) { }

    ngOnInit() {
        if (this.userService.active === true) {
            this.router.navigateByUrl('/analytics');
        }
    }

    submit() {
        // this.message = this.userService.submitLog(this.storage);
       this.authService.login(this.storage.username, this.storage.password).subscribe(res => {
        //    console.log('res', res);
            this.router.navigateByUrl('/analytics');
       },(error) => {
           this.message = error.json()['message'];
       });
    }

}