import { Component, OnInit } from '@angular/core';
import { Contest } from '../../model/contest.model';
import { League } from '../../model/league.model';
import { Observable } from 'rxjs/Observable';
import { LeagueService } from '../../services/league.service';
import { ContestService } from '../../services/contest.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-create-contest',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CreateContestComponent implements OnInit {
    public contest: Contest;
    public leagues: Observable<League []>; 
    public selectedLeague : League;
    public message: string;
    constructor(
        private leagueService: LeagueService,
        private contestService: ContestService,
        private router: Router
    ) { 
        this.leagues = this.leagueService.leagueList();
    }

    ngOnInit() {
        this.leagueService.loadAllLeagues();
        this.contest = new Contest;
        
        this.contest.tournament =  true;

        this.contest.name =  '';
        this.contest.minEntries =  0;
        this.contest.maxEntries = 0;
        this.contest.maxEntriesPerPlayer =  2;
        this.contest.entryFee =  0;
        this.contest.minPrizePool = 0;
        this.contest.prizeStructure =  '';
        this.contest.leagueId =  0;
        this.contest.prizes = [];            
        
     }
    
    addPrize() {
        this.contest['prizes']
        .push({
            fromRank: 0,
            toRank: 0,
            shareOfPrizePool: 0
        });
    }
    deletePrize(prize) {
        this.contest.prizes = this.contest.prizes.filter(item => item != prize)
    }

    saveContest() {

        this.contest.leagueId = this.selectedLeague.id;
        this.contest.periodId = this.selectedLeague.nextPeriodId;
        
        console.log(this.contest);
        this.contestService.submit(this.contest).subscribe(contest => {
            this.router.navigateByUrl('/contests');
        });
    }
    cancelChanges() {
        this.router.navigateByUrl('/contests');
    }
}