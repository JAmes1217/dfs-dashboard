import { Routes, RouterModule } from '@angular/router';
import { ContestListComponent } from './contest-list/contest-list.component';
import { NgModule } from '@angular/core';
import { ContestEditComponent } from './contest-edit/contest-edit.component';
import { CreateContestComponent } from './create/create.component';
import { ContestResolve } from './contest.resolver';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: ContestListComponent,
                data: {
                    title: 'Contest List'
                }
            },
            {
                path: 'create',
                component: CreateContestComponent,
                data: {
                    title: 'Create Contest'
                }
            },
            {
                path: 'edit/:id',
                component: ContestEditComponent,
                data: {
                    title: 'Contest Edit'
                },
                resolve: {
                    contestInfo: ContestResolve
                }
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ContestRoutingModule { }