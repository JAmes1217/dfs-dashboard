
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ContestRoutingModule } from './contest-routing.module';
import { ContestListComponent } from './contest-list/contest-list.component';
import { ContestService } from '../services/contest.service';
import { ContestEditComponent } from './contest-edit/contest-edit.component';
import { CreateContestComponent } from './create/create.component';
import { ContestResolve } from './contest.resolver';

@NgModule({
    imports: [ContestRoutingModule, CommonModule, FormsModule],
    exports: [],
    declarations: [ContestListComponent, ContestEditComponent,CreateContestComponent],
    providers: [ContestService, ContestResolve]
})

export class ContestModule { }