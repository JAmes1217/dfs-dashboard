import { ContestService } from '../../services/contest.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Contest } from '../../model/contest.model';


@Component({
    selector: 'app-contest-edit',
    templateUrl: 'contest-edit.component.html',
    styleUrls: ['./contest-edit.component.scss']
})
export class ContestEditComponent implements OnInit {
    public contestInfo: Contest;
    public contestInfoConstantValue: any;
    public message: string;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private contestService: ContestService
    ) {
        this.contestInfo = this.route.snapshot.data['contestInfo'];
        this.contestInfoConstantValue = JSON.stringify(this.route.snapshot.data['contestInfo']);
    }
    ngOnInit() {
        console.log(this.contestInfo);
    }
    compareJSON(obj1, obj2) {
        const result = {};
        for (const key in obj1) {
            if (obj2[key] !== obj1[key]) {
                result[key] = obj2[key];
            } else if (obj2[key] instanceof Array &&  obj1[key] instanceof Array) {
                result[key] = arguments.callee(obj1[key], obj2[key]);
            } else if (obj2[key] instanceof Object &&  obj1[key] instanceof Object) {
                result[key] = arguments.callee(obj1[key], obj2[key]);
            }
        }

        return result;
    }
    addPrize() {
        this.contestInfo['maxPrizeTable']
        .push({
            fromRank: 0,
            toRank: 0,
            shareOfPrizePool: 0
        });
    }

    saveChanges() {
        const compareData = this.compareJSON(JSON.parse(this.contestInfoConstantValue), this.contestInfo);
        if(compareData['maxPrizeTable']) {
            compareData['prize'] = compareData['maxPrizeTable'];
            delete compareData['maxPrizeTable'];
            delete compareData['currentPrizeTable'];
        }
        this.contestService.updateConstest(compareData, this.contestInfo.id).subscribe((res) => {
            // console.log(res);
            this.message = 'Contest Details was successfully updated.';
        }
        , (err) => {
            this.message = 'Error Updating Contest Details.';
        });
    }

    cancelChanges() {
        this.router.navigateByUrl('/contests');
    }
}