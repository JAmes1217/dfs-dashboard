import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Contest } from '../model/contest.model';
import { Observable } from 'rxjs/Observable';
import { ContestService } from '../services/contest.service';
import { Injectable } from '@angular/core';


@Injectable()
export class ContestResolve implements Resolve<Contest> {

    constructor(
        private contestService: ContestService
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Contest | Observable<Contest> | Promise<Contest> {
       return this.contestService.getContestById(route.params.id);
    }
}