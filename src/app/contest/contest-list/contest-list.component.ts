import { OnInit, Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Contest } from '../../model/contest.model';
import { Router } from '@angular/router';
import { ContestService } from '../../services/contest.service';
import { League } from '../../model/league.model';
import { LeagueService } from '../../services/league.service';

@Component({
    selector: 'app-contest-list',
    templateUrl: 'contest-list.components.html',
    styleUrls: ['./contest-list.components.scss']
})

export class ContestListComponent implements OnInit {
    public contest: Observable<Contest []>;
    public leagues: Observable<League []>;
    public page: number;
    public filterLeagueId: number;
    public filterPeriod: string;
    constructor(
        private router: Router,
        private contestService: ContestService,
        private leagueService: LeagueService
    ) {
        this.contest = this.contestService.contestList();
        this.leagues = this.leagueService.leagueList();
        this.page = 1;
        this.filterLeagueId = 0;
        this.filterPeriod = '';
    }
    ngOnInit() {
        this.contestService.loadAllContest('/contests?size=30&current=true');
        this.leagueService.loadAllLeagues();
    }

    viewContest(id: number) {
        this.router.navigateByUrl(`/contests/edit/${id}`);
    }

    triggerPage(todo: string) {
        let page = this.page;
        let byLeague = '';

        page = todo === 'add' ? page + 1 : page - 1;
        this.page = page;
        byLeague = this.filterLeagueId > 0 ? `&leagueId=${this.filterLeagueId}` : '';

        this.contestService.loadAllContest('/contests?current=true&size=9999&page=' + page + byLeague);
    }

    triggerFilter(by: string) {
        let path = '';
        if (by === 'league') {
            this.filterPeriod = '';
            path = `/contests?leagueId=${this.filterLeagueId}&size=9999&current=true`;
            this.contestService.loadAllContest(path);
        } else {
            const periodId = this.leagueService.getPeriodId(this.filterPeriod, this.filterLeagueId);
            if (periodId) {
                path = `/contests?periodId=${periodId}&size=9999`;
                this.contestService.loadAllContest(path);
            } else {
                // alert('No Period Id');
                this.contestService.showEmptyContent();
            }
        }
    }
}