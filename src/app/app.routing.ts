import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { AuthGuard } from './services/guard.service';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'players',
        loadChildren: './players/players.module#PlayersModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'games',
        loadChildren: './games/game.module#GameModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'contests',
        loadChildren: './contest/contest.module#ContestModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'teams',
        loadChildren: './team/team.module#TeamsModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'analytics',
        loadChildren: './analytics/analytics.module#AnalyticsModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'house-players',
        loadChildren: './house-players/house-players.module#HousePlayersModule'
      },
      {
        path: 'announcement',
        loadChildren: './announcement/announcement.module#AnnouncementModule',
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'login',
    component: SimpleLayoutComponent,
    data: {
      title: 'Login'
    },
    children: [
      {
        path: '',
        loadChildren: './login/login.module#LoginModule'
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
